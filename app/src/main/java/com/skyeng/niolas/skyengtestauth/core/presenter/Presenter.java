package com.skyeng.niolas.skyengtestauth.core.presenter;

import com.skyeng.niolas.skyengtestauth.core.view.IView;

/**
 * Created by lauraldo on 26.12.16.
 */

public abstract class Presenter<V extends IView> {

    private V view;

    public void attachView(V view) {
        this.view = view;
    }

    public abstract void initView();

    public void detachView() {
        view = null;
    }

    public V getView() {
        return view;
    }

}
