package com.skyeng.niolas.skyengtestauth.core;

import android.app.Application;

import com.skyeng.niolas.skyengtestauth.core.di.AppModule;
import com.skyeng.niolas.skyengtestauth.core.di.DaggerAppComponent;
import com.skyeng.niolas.skyengtestauth.core.di.DaggerService;

/**
 * Created by lauraldo on 27.12.16.
 */

public class SkyEngApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
    }

    private void initDagger() {
        DaggerService.initAppComponent(DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build());
    }
}
