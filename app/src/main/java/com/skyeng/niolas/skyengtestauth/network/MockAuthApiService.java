package com.skyeng.niolas.skyengtestauth.network;

import android.content.Context;

import com.skyeng.niolas.skyengtestauth.core.di.AppComponent;
import com.skyeng.niolas.skyengtestauth.core.di.DaggerService;
import com.skyeng.niolas.skyengtestauth.network.exception.ConnectionLossException;
import com.skyeng.niolas.skyengtestauth.network.exception.InvalidLoginOrPasswordException;
import com.skyeng.niolas.skyengtestauth.network.exception.InvalidSmsCodeException;
import com.skyeng.niolas.skyengtestauth.network.res.SmsCodeResponseMap;
import com.skyeng.niolas.skyengtestauth.utils.NetworkUtils;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

/**
 * Created by lauraldo on 27.12.16.
 */

public class MockAuthApiService implements AuthApiService {

    private static final String MOCK_JWT_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiTGF1cmFsZG8iLCJwaG90byI6IiJ9.j4EKiheMMbxQlc_wsEXbngGsdKi4NUX9WE_FSteHCRw";
    private static final String MOCK_REQUEST_ID = "6e895b28-b2a7-4db1-8259-87b22aab4ae4";
    private static final String MOCK_PHONE_PROMPT = "+7916*****89";
    private static final String MOCK_SMS_CODE = "12345";

    private static final String MESSAGE_WITH_PASSWORD = "Неверный адрес электронной почты или пароль";
    private static final String MESSAGE_WO_PASSWORD = "Неверный адрес электронной почты";
    private static final String MESSAGE_INVALID_SMS_CODE = "Неверный код";
    private static final String MESSAGE_NOT_CONNECTED = "Не удалось подключиться. Проверьте интернет-соединение.";

    private static final String MOCK_LOGIN = "superhero@example.com";
    private static final String MOCK_PASSWORD = "1234";

    private static final int DELAY_MILLIS = 2000;

    @Inject
    Context context;

    @Inject
    public MockAuthApiService() {
        AppComponent appComponent = DaggerService.getComponent(AppComponent.class);
        DaggerService.getComponent(Component.class, appComponent, new NetworkModule()).inject(this);
    }

    //region ================================== API stub ==================================

    @Override
    public Observable<String> signIn(String login, String password) {

        if (!NetworkUtils.isNetworkAvailable(context)) {
            return Observable.error(new ConnectionLossException(MESSAGE_NOT_CONNECTED));
        }

        return Observable.create(e -> {
            Thread.sleep(DELAY_MILLIS);
            if (login.equalsIgnoreCase(MOCK_LOGIN) && password.equals(MOCK_PASSWORD)) {
                e.onNext(MOCK_JWT_TOKEN);
            } else {
                e.onError(new InvalidLoginOrPasswordException(MESSAGE_WITH_PASSWORD));
            }
            e.onComplete();
        });
    }

    @Override
    public Observable<SmsCodeResponseMap> requestSmsCode(String login) {

        if (!NetworkUtils.isNetworkAvailable(context)) {
            return Observable.error(new ConnectionLossException(MESSAGE_NOT_CONNECTED));
        }

        SmsCodeResponseMap responseMap = new SmsCodeResponseMap();
        responseMap.request_id = MOCK_REQUEST_ID;
        responseMap.phone = MOCK_PHONE_PROMPT;

        return Observable.create(e -> {
            Thread.sleep(DELAY_MILLIS);
            if (login.equalsIgnoreCase(MOCK_LOGIN)) {
                e.onNext(responseMap);
            } else {
                e.onError(new InvalidLoginOrPasswordException(MESSAGE_WO_PASSWORD));
            }
            e.onComplete();
        });
    }

    @Override
    public Observable<String> verifySmsCode(String requestId, String code) {

        if (!NetworkUtils.isNetworkAvailable(context)) {
            return Observable.error(new ConnectionLossException(MESSAGE_NOT_CONNECTED));
        }

        return Observable.create(e -> {
            Thread.sleep(DELAY_MILLIS);
            if (requestId.equals(MOCK_REQUEST_ID) && (code.equals(MOCK_SMS_CODE))) {
                e.onNext(MOCK_JWT_TOKEN);
            } else {
                e.onError(new InvalidSmsCodeException(MESSAGE_INVALID_SMS_CODE));
            }
            e.onComplete();
        });
    }

    @Override
    public Completable signOut(String jwtToken) {

        if (!NetworkUtils.isNetworkAvailable(context)) {
            return Completable.error(new ConnectionLossException(MESSAGE_NOT_CONNECTED));
        }

        return Completable.complete().delay(DELAY_MILLIS, TimeUnit.MILLISECONDS);
    }

    //endregion

    //region ================================== DI ==================================

    @dagger.Component(dependencies = AppComponent.class, modules = NetworkModule.class)
    @ApiScope
    public interface Component {
        void inject(MockAuthApiService authApiService);
    }

    //endregion

}
