package com.skyeng.niolas.skyengtestauth.storage;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by lauraldo on 26.12.16.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface StorageScope {
}
