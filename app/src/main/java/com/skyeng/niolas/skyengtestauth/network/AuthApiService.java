package com.skyeng.niolas.skyengtestauth.network;

import com.skyeng.niolas.skyengtestauth.network.res.SmsCodeResponseMap;

import io.reactivex.Completable;
import io.reactivex.Observable;

/**
 * Created by lauraldo on 27.12.16.
 */

public interface AuthApiService {

    Observable<String> signIn(String login, String password);

    Observable<SmsCodeResponseMap> requestSmsCode(String login);

    Observable<String> verifySmsCode(String requestId, String code);

    Completable signOut(String jwtToken);

}
