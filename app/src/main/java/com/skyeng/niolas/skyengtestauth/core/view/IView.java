package com.skyeng.niolas.skyengtestauth.core.view;

/**
 * Created by lauraldo on 26.12.16.
 */

public interface IView {

    void showMessage(String message);

    void showError(Throwable throwable);

    void showProgress(String progressMessage);

    void hideProgress();

}
