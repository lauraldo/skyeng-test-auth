package com.skyeng.niolas.skyengtestauth.core.di;

import android.support.annotation.NonNull;
import android.util.Log;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by lauraldo on 26.12.16.
 */

public class DaggerService {

    private static final String TAG = "DAGGER_SERVICE";

    private static volatile Map<Class, Object> sComponentMap = new HashMap<>();

    public static void initAppComponent(@NonNull AppComponent appComponent) {
        sComponentMap.put(AppComponent.class, appComponent);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getComponent(Class<T> componentClass, Object... dependencies) {
        Object component = sComponentMap.get(componentClass);

        if (component == null) {
            component = createComponent(componentClass, dependencies);
            registerComponent(componentClass, component);
            Log.e(TAG, String.format("getComponent: created new component %s", componentClass.getName()));
        }

        return (T) component;
    }

    private static <T> T createComponent(Class<T> componentClass, Object... dependencies) {

        String fqn = componentClass.getName();

        String packageName = componentClass.getPackage().getName();
        // Accounts for inner classes, ie MyApplication$Component
        String simpleName = fqn.substring(packageName.length() + 1);
        String generatedName = (packageName + ".Dagger" + simpleName).replace('$', '_');

        try {
            Class<?> generatedClass = Class.forName(generatedName);
            Object builder = generatedClass.getMethod("builder").invoke(null);

            for (Method method : builder.getClass().getMethods()) {
                Class<?>[] params = method.getParameterTypes();
                if (params.length == 1) {
                    Class<?> dependencyClass = params[0];
                    for (Object dependency : dependencies) {
                        if (dependencyClass.isAssignableFrom(dependency.getClass())) {
                            method.invoke(builder, dependency);
                            break;
                        }
                    }
                }
            }
            //noinspection unchecked
            return (T) builder.getClass().getMethod("build").invoke(builder);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void registerComponent(Class componentClass, Object daggerComponent) {
        sComponentMap.put(componentClass, daggerComponent);
    }

    public static void killScope(Class<? extends Annotation> scopeAnnotation) {
        Iterator<Map.Entry<Class, Object>> iterator = sComponentMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Class, Object> entry = iterator.next();
            if (entry.getKey().isAnnotationPresent(scopeAnnotation)) {
                iterator.remove();
                Log.d(TAG, "killScope: scope killed");
            }
        }
    }

}
