package com.skyeng.niolas.skyengtestauth.auth;

import com.skyeng.niolas.skyengtestauth.core.di.DaggerService;
import com.skyeng.niolas.skyengtestauth.network.ApiScope;
import com.skyeng.niolas.skyengtestauth.network.AuthApiService;
import com.skyeng.niolas.skyengtestauth.network.NetworkModule;
import com.skyeng.niolas.skyengtestauth.storage.DataManager;
import com.skyeng.niolas.skyengtestauth.storage.StorageModule;
import com.skyeng.niolas.skyengtestauth.storage.StorageScope;

import javax.inject.Inject;

/**
 * Created by lauraldo on 27.12.16.
 */

public class AuthModel {

    private IAuthPresenter presenter;

    @Inject
    DataManager dataManager;

    @Inject
    AuthApiService apiService;

    @Inject
    AuthModel() {
    }

    AuthModel(IAuthPresenter authPresenter) {
        DaggerService.getComponent(Component.class,
                new StorageModule(),
                new NetworkModule())
                .inject(this);
        presenter = authPresenter;
    }

    public String getToken() {
        return dataManager.getToken();
    }

    public void setToken(String token) {
        dataManager.setToken(token);
    }

    public String getRequestId() {
        return dataManager.getRequestId();
    }

    public void setRequestId(String requestId) {
        dataManager.setRequestId(requestId);
    }

    public String getPhoneHint() {
        return dataManager.getPhoneHint();
    }

    public void setPhoneHint(String phoneHint) {
        dataManager.setPhoneHint(phoneHint);
    }

    public void signIn(String login, String password) {
        presenter.standardLoginPerformed(apiService.signIn(login, password));
    }

    public void requestSmsCode(String login) {
        presenter.smsCodeRequested(apiService.requestSmsCode(login));
    }

    public void verifySmsCode(String requestId, String code) {
        presenter.smsVerificationPerformed(apiService.verifySmsCode(requestId, code));
    }

    public void signOut(String jwtToken) {
        presenter.signOutPerformed(apiService.signOut(jwtToken));
    }

    //region ================================== DI ==================================

    @dagger.Component(modules = {StorageModule.class, NetworkModule.class})
    @AuthScope
    @ApiScope
    @StorageScope
    public interface Component {
        void inject(AuthModel authModel);
    }

    //endregion

}
