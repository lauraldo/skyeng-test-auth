package com.skyeng.niolas.skyengtestauth.network.exception;

/**
 * Created by lauraldo on 27.12.16.
 */

public class ConnectionLossException extends Exception {

    private String message;

    public ConnectionLossException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
