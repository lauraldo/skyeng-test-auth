package com.skyeng.niolas.skyengtestauth.auth;

import android.support.annotation.IntDef;

/**
 * Created by lauraldo on 28.12.16.
 */

@IntDef({AuthCodeStep.CODE_REQUEST, AuthCodeStep.CODE_VERIFY})
public @interface AuthCodeStep {

    int CODE_REQUEST = 0;
    int CODE_VERIFY = 1;

}
