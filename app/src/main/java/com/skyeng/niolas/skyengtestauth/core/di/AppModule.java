package com.skyeng.niolas.skyengtestauth.core.di;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lauraldo on 26.12.16.
 */

@Module
public class AppModule {

    private final Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    @NonNull
    Context provideContext() {
        return context;
    }
}
