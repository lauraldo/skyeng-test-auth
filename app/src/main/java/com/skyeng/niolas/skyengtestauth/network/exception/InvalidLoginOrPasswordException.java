package com.skyeng.niolas.skyengtestauth.network.exception;

/**
 * Created by lauraldo on 27.12.16.
 */

public class InvalidLoginOrPasswordException extends Exception {

    private String message;

    public InvalidLoginOrPasswordException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
