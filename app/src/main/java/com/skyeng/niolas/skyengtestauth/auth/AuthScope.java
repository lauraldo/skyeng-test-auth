package com.skyeng.niolas.skyengtestauth.auth;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by lauraldo on 27.12.16.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthScope {
}
