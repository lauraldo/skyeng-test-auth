package com.skyeng.niolas.skyengtestauth.core;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

/**
 * Created by lauraldo on 28.12.16.
 */

public class ConsoleRun {

    public static void main(String[] args) {
        Observable.interval(0, 1, TimeUnit.SECONDS)
                .map(i -> 60 - i)
                .take(61)
                .subscribe(sec -> {
                    System.out.println(String.valueOf(sec));
                });
        while (true) {

        }
    }

}
