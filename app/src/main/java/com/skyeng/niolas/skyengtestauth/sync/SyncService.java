package com.skyeng.niolas.skyengtestauth.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.skyeng.niolas.skyengtestauth.BuildConfig;
import com.skyeng.niolas.skyengtestauth.core.di.DaggerService;
import com.skyeng.niolas.skyengtestauth.storage.DataManager;
import com.skyeng.niolas.skyengtestauth.storage.StorageModule;
import com.skyeng.niolas.skyengtestauth.storage.StorageScope;
import com.skyeng.niolas.skyengtestauth.storage.SyncData;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class SyncService extends Service {

    private static boolean running = false;

    private Disposable timer;

    @Inject
    DataManager dataManager;

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerService.getComponent(Component.class, new StorageModule()).inject(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        running = true;
        if (timer == null || timer.isDisposed()) {
            timer = Observable.interval(0, 24, TimeUnit.HOURS)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tick -> {
                        sync(dataManager.getToken());
                    });
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        if (timer != null && !timer.isDisposed()) {
            timer.dispose();
        }
        running = false;
        super.onDestroy();
    }

    public static boolean isRunning() {
        return running;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void sync(String jwtToken) {
        SyncData data = new SyncData();
        // TODO: 28.12.16 Sync data for real
        dataManager.dataSync(data);
        if (BuildConfig.DEBUG) {
            Log.d(SyncService.class.getSimpleName(), "Synced.");
        }
    }

    //region ================================== DI ==================================

    @dagger.Component(modules = StorageModule.class)
    @StorageScope
    public interface Component {
        void inject(SyncService syncService);
    }

    //endregion

}
