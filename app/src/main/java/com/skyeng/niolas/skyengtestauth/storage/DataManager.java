package com.skyeng.niolas.skyengtestauth.storage;

import android.content.Context;

import com.skyeng.niolas.skyengtestauth.core.di.AppComponent;
import com.skyeng.niolas.skyengtestauth.core.di.DaggerService;

import javax.inject.Inject;

/**
 * Created by lauraldo on 26.12.16.
 */

public class DataManager {

    @Inject
    Context context;

    @Inject
    SharedPrefsManager sharedPrefsManager;

    @Inject
    public DataManager() {
        AppComponent appComponent = DaggerService.getComponent(AppComponent.class);
        DaggerService.getComponent(Component.class, appComponent, new StorageModule()).inject(this);
    }

    public void setToken(String token) {
        sharedPrefsManager.saveToken(token);
    }

    public String getToken() {
        return sharedPrefsManager.loadToken();
    }

    public void setRequestId(String requestId) {
        sharedPrefsManager.saveRequestId(requestId);
    }

    public String getRequestId() {
        return sharedPrefsManager.loadRequestId();
    }

    public void setPhoneHint(String phoneHint) {
        sharedPrefsManager.savePhoneHint(phoneHint);
    }

    public String getPhoneHint() {
        return sharedPrefsManager.loadPhoneHint();
    }

    public void dataSync(SyncData data) {
        // TODO: 28.12.16 store synced data
    }

    //region ================================== DI ==================================

    @dagger.Component(dependencies = AppComponent.class, modules = StorageModule.class)
    @StorageScope
    public interface Component {
        void inject(DataManager dataManager);
    }

    //endregion

}
