package com.skyeng.niolas.skyengtestauth.core.di;

import android.content.Context;

import com.skyeng.niolas.skyengtestauth.storage.SharedPrefsManager;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by lauraldo on 26.12.16.
 */

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    Context getContext();

    void inject(SharedPrefsManager sharedPrefsManager);

}
