package com.skyeng.niolas.skyengtestauth.storage;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lauraldo on 26.12.16.
 */

@Module
public class StorageModule {

    @Provides
    @StorageScope
    SharedPrefsManager provideSharedPrefsManager() {
        return new SharedPrefsManager();
    }

    @Provides
    @StorageScope
    DataManager provideDataManager() {
        return new DataManager();
    }

}
