package com.skyeng.niolas.skyengtestauth.auth;

import com.skyeng.niolas.skyengtestauth.network.res.SmsCodeResponseMap;

import io.reactivex.Completable;
import io.reactivex.Observable;

/**
 * Created by lauraldo on 27.12.16.
 */

public interface IAuthPresenter {

    boolean userSigned();
    String getPhoneHint();

    void performLogin();
    void verifyCode();
    void signOut();
    void requestCodeAgain();

    void standardLoginPerformed(Observable<String> tokenInfo);
    void smsCodeRequested(Observable<SmsCodeResponseMap> smsCodeInfo);
    void smsVerificationPerformed(Observable<String> requestInfo);
    void signOutPerformed(Completable signOut);

    boolean isEmailValid(String text);
    boolean isPasswordValid(String text);
    boolean isCodeValid(String text);

}
