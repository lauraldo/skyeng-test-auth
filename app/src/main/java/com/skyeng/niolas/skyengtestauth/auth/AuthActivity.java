package com.skyeng.niolas.skyengtestauth.auth;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;

import com.skyeng.niolas.skyengtestauth.R;
import com.skyeng.niolas.skyengtestauth.core.di.DaggerService;
import com.skyeng.niolas.skyengtestauth.core.view.BaseActivity;
import com.skyeng.niolas.skyengtestauth.databinding.ActivityAuthBinding;
import com.skyeng.niolas.skyengtestauth.sync.SyncService;

import javax.inject.Inject;

import dagger.Provides;

public class AuthActivity extends BaseActivity implements IAuthView, View.OnClickListener {

    private static final String SAVED_STATE_KEY = "SAVED_STATE_KEY";

    private ActivityAuthBinding binding;

    private int authMode = -1;
    private int authCodeStep;
    private boolean signed;
    private boolean emailValid = false;
    private boolean passwordValid = false;
    private boolean codeValid = false;

    @Inject
    AuthPresenter presenter;

    //region ================================== Lifecycle ==================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBinding(DataBindingUtil.setContentView(this, R.layout.activity_auth));
        binding = (ActivityAuthBinding) getBinding();

        DaggerService.getComponent(Component.class, new Module()).inject(this);

        if (savedInstanceState != null) {
            SavedState state = savedInstanceState.getParcelable(SAVED_STATE_KEY);
            if (state != null) {
                setSignedState(state.signed);
                if (!this.signed) {
                    setAuthMode(state.authMode);
                    if (authMode == AuthMode.CODE) {
                        if (state.authCodeStep == AuthCodeStep.CODE_REQUEST) {
                            returnToEntering();
                        } else {
                            switchToVerifyCode();
                        }
                    }
                }
            } else {
                setSignedState(presenter.userSigned());
                setAuthMode(AuthMode.PASSWORD);
            }
        } else {
            setSignedState(presenter.userSigned());
            setAuthMode(AuthMode.PASSWORD);
        }

        setupActionBar();

        setupViewState();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(SAVED_STATE_KEY,
                new SavedState(authMode, authCodeStep, signed, emailValid, passwordValid, codeValid));
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
        presenter.initView();
    }

    @Override
    protected void onStop() {
        presenter.detachView();
        super.onStop();
    }

    //endregion

    //region ================================== IAuthView ==================================

    @Override
    public void setAuthMode(int authMode) {

        if (this.authMode == authMode) {
            return;
        }

        this.authMode = authMode;

        binding.editPassword.setVisibility(authMode == AuthMode.PASSWORD
                ? View.VISIBLE : View.GONE);
        binding.authBtn.setText(authMode == AuthMode.PASSWORD
                ? R.string.enter : R.string.request_auth_code);
        binding.switchMode.setText(authMode == AuthMode.PASSWORD
                ? R.string.auth_wo_password : R.string.auth_via_password);
        binding.hintText.setText(authMode == AuthMode.PASSWORD
                ? R.string.switch_password_hint : R.string.switch_code_hint);

        if (authCodeStep == AuthCodeStep.CODE_VERIFY) {
            authCodeStep = AuthCodeStep.CODE_REQUEST;
            binding.flipper.showPrevious();
        }
    }

    @Override
    public void switchToVerifyCode() {
        if (authCodeStep != AuthCodeStep.CODE_VERIFY) {
            binding.flipper.showNext();
        }
        binding.phoneHintText.setText(String.format(getString(R.string.enter_code_prompt),
                presenter.getPhoneHint()));
        authCodeStep = AuthCodeStep.CODE_VERIFY;
        setupActionBar();
    }

    @Override
    public void returnToEntering() {
        if (authCodeStep != AuthCodeStep.CODE_REQUEST) {
            binding.flipper.showPrevious();
        }
        authCodeStep = AuthCodeStep.CODE_REQUEST;
        setupActionBar();
    }

    @Override
    public void setSignedState(boolean signed) {
        this.signed = signed;

        binding.editEmail.setText("");
        binding.editPassword.setText("");
        binding.editCode.setText("");

        binding.flipper.setVisibility(signed ? View.GONE : View.VISIBLE);
        binding.mainContainer.setVisibility(signed ? View.VISIBLE : View.GONE);

        if (signed) {
            refreshSyncService();
        } else {
            if (SyncService.isRunning()) {
                this.stopService(new Intent(this, SyncService.class));
            }
        }
    }

    @Override
    public void setCodeRemainingSeconds(long sec) {
        if ((authMode == AuthMode.CODE) && (authCodeStep == AuthCodeStep.CODE_VERIFY)) {
            if (sec == 0) {
                binding.requestCodeAgain.setText(R.string.request_code_again);
                binding.requestCodeAgain.setClickable(true);
                binding.requestCodeAgain.setTextColor(getResources().getColor(R.color.brandBlue));
            } else {
                binding.requestCodeAgain.setClickable(false);
                binding.requestCodeAgain.setText(String.format(getString(R.string.request_code_again_sec),
                        String.valueOf(sec)));
                binding.requestCodeAgain.setTextColor(getResources().getColor(R.color.brandBlueTransparent));
            }
        }
    }


    @Override
    public int getAuthMode() {
        return authMode;
    }

    @Override
    public String getEmailText() {
        return binding.editEmail.getText().toString();
    }

    @Override
    public String getPasswordText() {
        return binding.editPassword.getText().toString();
    }

    @Override
    public String getCodeText() {
        return binding.editCode.getText().toString();
    }

    //endregion

    //region ================================== DI ==================================

    @dagger.Module
    public static class Module {

        @Provides
        @AuthScope
        AuthPresenter provideAuthPresenter() {
            return new AuthPresenter();
        }

    }

    @dagger.Component(modules = Module.class)
    @AuthScope
    public interface Component {
        void inject(AuthActivity authActivity);
    }

    //endregion

    private void refreshSyncService() {
        if (!SyncService.isRunning()) {
            this.startService(new Intent(this, SyncService.class));
        }
    }

    private void setupViewState() {
        binding.switchMode.setOnClickListener(this);
        binding.authBtn.setOnClickListener(this);
        binding.verifyCodeBtn.setOnClickListener(this);
        binding.logoutBtn.setOnClickListener(this);
        binding.requestCodeAgain.setOnClickListener(this);

        binding.editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setEmailVaildityState(presenter.isEmailValid(editable.toString()));
            }
        });
        binding.editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setPasswordValidityState(presenter.isPasswordValid(editable.toString()));
            }
        });
        binding.editCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setCodeValidityState(presenter.isCodeValid(editable.toString()));
            }
        });

        setEmailVaildityState(emailValid);
        setPasswordValidityState(passwordValid);
        setCodeValidityState(codeValid);
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        setActionBarTitle(authMode == AuthMode.CODE && authCodeStep == AuthCodeStep.CODE_VERIFY
                ? getString(R.string.enter_code)
                : getString(R.string.entrance));
    }

    private void setActionBarTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.switchMode:
                setAuthMode(authMode == AuthMode.PASSWORD ? AuthMode.CODE : AuthMode.PASSWORD);
                break;
            case R.id.authBtn:
                presenter.performLogin();
                break;
            case R.id.verifyCodeBtn:
                presenter.verifyCode();
                break;
            case R.id.logoutBtn:
                presenter.signOut();
                break;
            case R.id.requestCodeAgain:
                presenter.requestCodeAgain();
                break;
        }
    }

    private void setEmailVaildityState(boolean valid) {
        emailValid = valid;
        if (AuthMode.PASSWORD == authMode) {
            binding.authBtn.setEnabled(emailValid && passwordValid);
        } else {
            binding.authBtn.setEnabled(emailValid);
        }
    }

    private void setPasswordValidityState(boolean valid) {
        passwordValid = valid;
        if (AuthMode.PASSWORD == authMode) {
            binding.authBtn.setEnabled(emailValid && passwordValid);
        } else {
            binding.authBtn.setEnabled(emailValid);
        }
    }

    private void setCodeValidityState(boolean valid) {
        codeValid = valid;
        binding.verifyCodeBtn.setEnabled(codeValid);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if ((authMode == AuthMode.CODE) && (authCodeStep == AuthCodeStep.CODE_VERIFY)) {
            returnToEntering();
        } else {
            finish();
        }
    }

    static class SavedState implements Parcelable {

        private int authMode;
        private int authCodeStep;
        private boolean signed;
        private boolean emailValid;
        private boolean passwordValid;
        private boolean codeValid;

        public static final Parcelable.Creator<SavedState> CREATOR = new Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            @Override
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        public SavedState(int authMode, int authCodeStep, boolean signed, boolean emailValid, boolean passwordValid, boolean codeValid) {
            this.authMode = authMode;
            this.authCodeStep = authCodeStep;
            this.signed = signed;
            this.emailValid = emailValid;
            this.passwordValid = passwordValid;
            this.codeValid = codeValid;
        }

        public SavedState(Parcel source) {
            authMode = source.readInt();
            authCodeStep = source.readInt();
            signed = source.readByte() != 0;
            emailValid = source.readByte() != 0;
            passwordValid = source.readByte() != 0;
            codeValid = source.readByte() != 0;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            out.writeInt(authMode);
            out.writeInt(authCodeStep);
            out.writeByte((byte) (signed ? 1 : 0));
            out.writeByte((byte) (emailValid ? 1 : 0));
            out.writeByte((byte) (passwordValid ? 1 : 0));
            out.writeByte((byte) (codeValid ? 1 : 0));
        }

    }

}
