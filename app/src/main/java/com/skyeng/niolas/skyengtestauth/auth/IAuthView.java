package com.skyeng.niolas.skyengtestauth.auth;

import com.skyeng.niolas.skyengtestauth.core.view.IView;

/**
 * Created by lauraldo on 27.12.16.
 */

public interface IAuthView extends IView {

    void setAuthMode(int authMode);

    void switchToVerifyCode();

    void returnToEntering();

    void setSignedState(boolean signed);

    void setCodeRemainingSeconds(long sec);

    int getAuthMode();

    String getEmailText();

    String getPasswordText();

    String getCodeText();

}
