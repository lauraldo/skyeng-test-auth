package com.skyeng.niolas.skyengtestauth.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.skyeng.niolas.skyengtestauth.core.di.AppComponent;
import com.skyeng.niolas.skyengtestauth.core.di.DaggerService;

import javax.inject.Inject;

/**
 * Created by lauraldo on 26.12.16.
 */

public class SharedPrefsManager {

    private static final String JWT_TOKEN_KEY = "JWT_TOKEN_KEY";
    private static final String REQUEST_ID_KEY = "REQUEST_ID_KEY";
    private static final String PHONE_HINT_KEY = "PHONE_HINT_KEY";

    private SharedPreferences sharedPreferences;

    @Inject
    Context context;

    @Inject
    public SharedPrefsManager() {
        DaggerService.getComponent(AppComponent.class).inject(this);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    void saveToken(String token) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(JWT_TOKEN_KEY, token);
        editor.apply();
    }

    String loadToken() {
        return sharedPreferences.getString(JWT_TOKEN_KEY, null);
    }

    void saveRequestId(String requestId) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(REQUEST_ID_KEY, requestId);
        editor.apply();
    }

    String loadRequestId() {
        return sharedPreferences.getString(REQUEST_ID_KEY, null);
    }

    void savePhoneHint(String phoneHint) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PHONE_HINT_KEY, phoneHint);
        editor.apply();
    }

    String loadPhoneHint() {
        return sharedPreferences.getString(PHONE_HINT_KEY, "");
    }

}
