package com.skyeng.niolas.skyengtestauth.network;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lauraldo on 27.12.16.
 */

@Module
public class NetworkModule {

    @Provides
    @ApiScope
    AuthApiService provideAuthApiService() {
        return new MockAuthApiService();
    }
}
