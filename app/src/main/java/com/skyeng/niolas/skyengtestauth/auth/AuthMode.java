package com.skyeng.niolas.skyengtestauth.auth;

import android.support.annotation.IntDef;

/**
 * Created by lauraldo on 26.12.16.
 */

@IntDef({AuthMode.PASSWORD, AuthMode.CODE})
public @interface AuthMode {

    int PASSWORD = 0;
    int CODE = 1;
}
