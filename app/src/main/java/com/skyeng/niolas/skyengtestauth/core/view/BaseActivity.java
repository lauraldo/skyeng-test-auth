package com.skyeng.niolas.skyengtestauth.core.view;

import android.app.ProgressDialog;
import android.databinding.ViewDataBinding;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by lauraldo on 26.12.16.
 */

public class BaseActivity extends AppCompatActivity implements IView {

    private ViewDataBinding dataBinding;

    private ProgressDialog progressDialog;

    public ViewDataBinding getBinding() {
        return dataBinding;
    }

    public void setBinding(ViewDataBinding binding) {
        dataBinding = binding;
    }

    //region ================================== IView ==================================

    @Override
    public void showMessage(String message) {
        Snackbar.make(dataBinding.getRoot(), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable throwable) {
        Snackbar.make(dataBinding.getRoot(), throwable.getMessage(), Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void showProgress(String progressMessage) {
        progressDialog = ProgressDialog.show(this, null, progressMessage, true, false);
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    //endregion

}
