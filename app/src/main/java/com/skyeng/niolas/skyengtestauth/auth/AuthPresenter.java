package com.skyeng.niolas.skyengtestauth.auth;

import android.text.TextUtils;
import android.util.Patterns;

import com.skyeng.niolas.skyengtestauth.core.di.DaggerService;
import com.skyeng.niolas.skyengtestauth.core.presenter.Presenter;
import com.skyeng.niolas.skyengtestauth.network.res.SmsCodeResponseMap;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.Provides;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by lauraldo on 27.12.16.
 */

public class AuthPresenter extends Presenter<IAuthView> implements IAuthPresenter {

    @Inject
    AuthModel model;

    Disposable timer;

    @Inject
    AuthPresenter() {
        DaggerService.getComponent(Component.class, new Module(this)).inject(this);
    }

    //region ================================== IAuthPresenter ==================================

    @Override
    public boolean userSigned() {
        return !TextUtils.isEmpty(model.getToken());
    }

    @Override
    public String getPhoneHint() {
        return model.getPhoneHint();
    }

    @Override
    public void performLogin() {
        if (getView() != null) {
            if (getView().getAuthMode() == AuthMode.PASSWORD) {
                getView().showProgress("Выполняется вход.\nПодождите…");
                model.signIn(getView().getEmailText(), getView().getPasswordText());
            } else {
                stopCodeRequestTimer();
                getView().showProgress("Отправка кода.\nПодождите…");
                model.requestSmsCode(getView().getEmailText());
            }
        }
    }

    @Override
    public void verifyCode() {
        stopCodeRequestTimer();
        if (getView() != null) {
            getView().showProgress("Выполняется вход.\nПодождите…");
            model.verifySmsCode(model.getRequestId(), getView().getCodeText());
        }
    }

    @Override
    public void signOut() {
        if (getView() != null) {
            getView().showProgress("Выполняется выход.\nПодождите…");
            model.signOut(model.getToken());
        }
    }

    @Override
    public void requestCodeAgain() {
        stopCodeRequestTimer();
        if (getView() != null) {
            getView().showProgress("Отправка кода.\nПодождите…");
            model.requestSmsCode(getView().getEmailText());
        }
    }

    private void stopCodeRequestTimer() {
        if (timer != null && !timer.isDisposed()) {
            timer.dispose();
        }
    }

    private void startCodeRequestTimer() {

        timer = Observable.interval(0, 1, TimeUnit.SECONDS)
                .map(i -> 60 - i)
                .take(61)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(sec -> {
                    if (getView() != null) {
                        getView().setCodeRemainingSeconds(sec);
                    }
                });
    }

    @Override
    public void standardLoginPerformed(Observable<String> tokenInfo) {
        if (getView() != null) {
            tokenInfo
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(s -> {
                        model.setToken(s);
                        getView().hideProgress();
                        getView().setSignedState(true);
                    }, throwable -> {
                        getView().hideProgress();
                        getView().showError(throwable);
                    });
        }
    }

    @Override
    public void smsCodeRequested(Observable<SmsCodeResponseMap> smsCodeInfo) {
        if (getView() != null) {
            smsCodeInfo
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(smsCodeResponseMap -> {
                        model.setRequestId(smsCodeResponseMap.request_id);
                        model.setPhoneHint(smsCodeResponseMap.phone);
                        getView().hideProgress();
                        getView().switchToVerifyCode();
                        startCodeRequestTimer();
                    }, throwable -> {
                        getView().hideProgress();
                        getView().showError(throwable);
                    });
        }
    }

    @Override
    public void smsVerificationPerformed(Observable<String> requestInfo) {
        if (getView() != null) {
            requestInfo
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(s -> {
                        model.setToken(s);
                        getView().hideProgress();
                        getView().setSignedState(true);
                    }, throwable -> {
                        getView().hideProgress();
                        getView().showError(throwable);
                    });
        }
    }

    @Override
    public void signOutPerformed(Completable signOut) {
        if (getView() != null) {
            signOut
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> {
                        getView().hideProgress();
                        model.setToken(null);
                        getView().setAuthMode(AuthMode.PASSWORD);
                        getView().setSignedState(false);
                    }, throwable -> {
                        getView().hideProgress();
                        getView().showError(throwable);
                    });
        }
    }

    @Override
    public boolean isEmailValid(String text) {
        return Patterns.EMAIL_ADDRESS.matcher(text.trim()).matches();
    }

    @Override
    public boolean isPasswordValid(String text) {
        return !TextUtils.isEmpty(text.trim());
    }

    @Override
    public boolean isCodeValid(String text) {
        return !TextUtils.isEmpty(text.trim());
    }

    //endregion

    //region ================================== Presenter ==================================

    @Override
    public void initView() {
    }

    //endregion

//region ================================== DI ==================================

    @dagger.Module
    public static class Module {

        private IAuthPresenter presenter;

        public Module(IAuthPresenter presenter) {
            this.presenter = presenter;
        }

        @Provides
        @AuthScope
        AuthModel provideAuthModel() {
            return new AuthModel(presenter);
        }
    }

    @dagger.Component(modules = Module.class)
    @AuthScope
    public interface Component {
        void inject(AuthPresenter authPresenter);
    }

//endregion

}
